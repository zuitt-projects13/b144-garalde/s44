// Get post data


fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((data) => showPosts(data))

// add post data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	// preventDefault prevents the page from loading
	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts', {
		// method to use to show the data from the placeholder
		method: 'POST',
		body: JSON.stringify({
			title : document.querySelector('#txt-title').value,
			body:document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers:{'Content-Type' : 'application/json; charset=UTF-8'}

	}).then((response) => response.json()) //return response in json data
	.then((data) => {
		console.log(data)
		alert('Successfully added post')

		document.querySelector('#txt-title').value = null
		document.querySelector('#txt-body').value = null
	})
})



// showpost


const showPosts = (posts) => {
	let postEntries =''

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div

		`
	})


	document.querySelector('#div-post-entries').innerHTML= postEntries
}




// Edit Post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	// removes the disabled attribute from the button
	document.querySelector('#btn-submit-update').removeAttribute('disabled')

}



// update post

document.querySelector('#form-edit-post').addEventListener('submit', (event) => {
	event.preventDefault()

	//we can only update the one we created since the placeholder server does not allow us to update the file /1
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		// method to use to show the data from the placeholder
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title : document.querySelector('#txt-edit-title').value,
			body:document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers:{'Content-Type' : 'application/json; charset=UTF-8'}

	}).then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert('Successfully updated post')

		document.querySelector('#txt-edit-id').value = null
		document.querySelector('#txt-edit-title').value = null
		document.querySelector('#txt-edit-body').value = null
		// setting back the disabled attribute to true
		document.querySelector('#btn-submit-update').setAttribute('disabled',true)

	})


})
	


const deletePost = (id)	=> {


	fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((result) => {

	result = result.filter((post) => {
        if (post.id.toString() !== id) {
            return post;
        }
    });

	document.querySelector(`#post-${id}`).remove();


	showPosts(result)
})

}